#!bin/bash


#echo "fastestmirror=true" >> /etc/dnf/dnf.conf
dnf install -y screen


dnf update -y
dnf install -y lorax* virt-install libvirt-daemon-config-network pykickstart anaconda @anaconda-tools screen git emacs mosh htop
setenforce 0

mkdir -p ~/ks ~/logs ~/iso && cd ~/ks
git clone https://gitlab.com/balls/fedora.git && cd fedora 

ksflatten -v, --config fedora-live-workstation.ks -o flat-fedora-live-workstation.ks --version F25
cp flat-fedora-live-workstation.ks ~/logs/ && cd ~/logs/

livemedia-creator --ks ~/logs/flat-fedora-live-workstation.ks --no-virt --resultdir /var/lmc --project fedora-workstation-Live --make-iso --volid fedora-workstation-f25 --iso-only --iso-name fedora-workstation-f25.iso --releasever 25 --title deadrat --nomacboot
