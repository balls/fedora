

#%include fedora-rpmfusion-free.ks
#%include fedora-rpmfusion-nonfree.ks

%packages




# i3 wm
i3
i3status
dmenu
i3lock
xbacklight
feh

#tools
conky
htop
cowsay
nmon
nethogs
p7zip
p7zip-gui
gparted
@admin-tools
wget
rfkill
screen
fpaste
terminator

#text 
emacs
emacs-auto-complete
geany
leafpad
meld


#for internet
midori
firefox
links
weechat
git
mosh
libreoffice

#media
gimp
cheese





%end
