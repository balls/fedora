# Include the appropriate repo definitions
# Adding RPMFusion Free to the list
repo --name="rpmfusion-nonfree" --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="rpmfusion-nonfree-updates" --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch/ --cost=1000

%include fedora-rpmfusion-free.ks

